package com.example.register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomePage extends AppCompatActivity {

    TextView textName, textEmail, textCity, textMajor;
    Button btnSite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcomepage);

        textName = findViewById(R.id.t_namee);
        textEmail = findViewById(R.id.t_emaill);
        textCity = findViewById(R.id.t_cityy);
        textMajor = findViewById(R.id.t_majorr);
        btnSite = findViewById(R.id.btn_ig);

        String KEY_NAME = "NAME";
        String tampilName = getIntent().getStringExtra(KEY_NAME);
        textName.setText(tampilName);

        String KEY_EMAIL = "EMAIL";
        String tampilEmail = getIntent().getStringExtra(KEY_EMAIL);
        textEmail.setText(tampilEmail);

        String KEY_CITY = "CITY";
        String tampilCity = getIntent().getStringExtra(KEY_CITY);
        textCity.setText(tampilCity);

        String KEY_MAJOR = "MAJOR";
        String tampilMajor = getIntent().getStringExtra(KEY_MAJOR);
        textMajor.setText(tampilMajor);


        btnSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.instagram.com/itcupnyk/?hl=id";
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);

            }
        });


    }
}
